package com.amazingciel.blogspringapp.config;

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.ArrayList;
import java.util.Collection;

@Configuration
@EnableMongoRepositories(basePackages = "com.amazingciel.blogspringapp.repository")
public class MongoConfig extends AbstractMongoConfiguration {

    @Override
    protected String getDatabaseName(){
        return "beanblog";
    }

    @Override
    public MongoClient mongoClient(){
        return new MongoClient("127.0.0.1", 27017);
    }

    @Override
    protected Collection<String> getMappingBasePackages(){
        ArrayList<String> packages = new ArrayList<String>();
        packages.add("com.amazingciel.blogspringapp");

        return packages;
    }
}
