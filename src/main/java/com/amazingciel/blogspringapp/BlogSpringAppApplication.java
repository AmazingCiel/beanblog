package com.amazingciel.blogspringapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogSpringAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlogSpringAppApplication.class, args);
    }

}
