package com.amazingciel.blogspringapp.controller;

import com.amazingciel.blogspringapp.model.Message;
import com.amazingciel.blogspringapp.repository.MessageRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Date;

@Controller
@RequestMapping(value = "/message")
public class MessageController {

    private final MessageRepository messageRepository;

    public MessageController(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String Add(@Valid @ModelAttribute("newMessage") Message message, BindingResult result, ModelMap model){
        if (result.hasErrors()){
            return "error";
        }

        message.setCreateDate(new Date());

        messageRepository.Save(message);

        return String.format("redirect:/topic/%s", message.getTopicId());
    }
}
