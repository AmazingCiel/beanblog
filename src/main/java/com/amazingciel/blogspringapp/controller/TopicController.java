package com.amazingciel.blogspringapp.controller;

import com.amazingciel.blogspringapp.model.Message;
import com.amazingciel.blogspringapp.model.Topic;
import com.amazingciel.blogspringapp.repository.MessageRepository;
import com.amazingciel.blogspringapp.repository.TopicRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequestMapping(value = "/")
public class TopicController {

    private final TopicRepository topicRepository;
    private final MessageRepository messageRepository;

    public TopicController(TopicRepository topicRepository, MessageRepository messageRepository) {
        this.topicRepository = topicRepository;
        this.messageRepository = messageRepository;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView All() {

        ModelAndView modelAndView = new ModelAndView("all-posts");

        modelAndView.addObject("topics", topicRepository.findAll());
        modelAndView.addObject("topicCount", topicRepository.Count());

        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView Add() {
        return new ModelAndView("add", "topic", new Topic());
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String Add(@Valid @ModelAttribute("topic") Topic topic,
                      @Valid @RequestParam("upload-image") MultipartFile file,
                      BindingResult result,
                      ModelMap model) throws IOException {
        if (result.hasErrors()) {
            return "error";
        }

        if (!file.isEmpty()){
            topicRepository.Save(topic, file);

        }
        else {
            topicRepository.Save(topic);
        }

        return "redirect:/success";
    }

    @RequestMapping(value = "/success", method = RequestMethod.GET)
    public String AddSucceed() {
        return "success";
    }

    @RequestMapping(value = "/topic/{id}")
    public ModelAndView Detail(@PathVariable("id") String id) {
        ModelAndView modelAndView = new ModelAndView("detail");

        Topic topic = topicRepository.findById(id);

        if (topic == null) {
            return new ModelAndView("error");
        }

        modelAndView.addObject("topic", topic);
        modelAndView.addObject("messages", messageRepository.getMessagesByTopicId(id));
        modelAndView.addObject("newMessage", new Message());

        return modelAndView;
    }
}
