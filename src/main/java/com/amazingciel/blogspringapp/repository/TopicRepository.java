package com.amazingciel.blogspringapp.repository;

import com.amazingciel.blogspringapp.model.Topic;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Date;

@Repository
public class TopicRepository {

    private final MongoOperations operations;
    private final ServletContext servletContext;

    public TopicRepository(MongoOperations operations, ServletContext context) {
        this.operations = operations;
        this.servletContext = context;
    }

    public Topic findById(String Id) {
        return operations.findById(Id, Topic.class);
    }

    public Collection<Topic> findAll() {
        return operations.find(new Query().with(new Sort(Sort.Direction.DESC, "createDate")), Topic.class);
    }

    public long Count() {
        return operations.findAll(Topic.class).size();
    }

    public void Save(Topic topic, MultipartFile file) throws IOException {
        topic.setCreateDate(new Date());
        operations.save(topic);

        File uploadLocation = new File(servletContext.getRealPath("/resources/img/topic-image"));
        File f = new File(uploadLocation, topic.getId() + file.getOriginalFilename());
        try(InputStream inputStream = file.getInputStream()) {
            Files.copy(inputStream, f.toPath());
            topic.setImageName(topic.getId() + file.getOriginalFilename());
        }

        operations.save(topic);
    }

    public void Save(Topic topic) {
        topic.setCreateDate(new Date());
        topic.setImageName("default-topic-image.png");
        operations.save(topic);
    }
}
