package com.amazingciel.blogspringapp.repository;

import com.amazingciel.blogspringapp.model.Message;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public class MessageRepository {

    private final MongoOperations operations;

    public MessageRepository(MongoOperations operations) {
        this.operations = operations;
    }

    public Collection<Message> getMessagesByTopicId(String topicId){
        Query query = new Query();
        query.addCriteria(Criteria.where("topicId").is(topicId));

        return operations.find(query.with(new Sort(Sort.Direction.DESC, "createDate")), Message.class);
    }

    public void Save(Message message){
        operations.save(message);
    }
}
