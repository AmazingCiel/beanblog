package com.amazingciel.blogspringapp.model;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Date;

@Document(collection = "topic")
public class Topic {

    @Id
    private String id;
    private String title;
    private String content;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date createDate;
    private String imageName;

    public String getContent() {
        return content;
    }

    public void setContent(String Content) {
        content = Content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String Title) {
        title = Title;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public String getFormatedDate() {
        return new SimpleDateFormat("dd MMM, yyyy").format(createDate);
    }

    public void setCreateDate(Date CreateDate) {
        createDate = CreateDate;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String ImagePath) {
        imageName = ImagePath;
    }

    @Override
    public String toString() {
        return String.format("Topic { Title = %s, Content = %s, CreateDate = %s, ImagePath = %s }",
                title, content, createDate.toString(), imageName);
    }

    public String getId() {
        return id;
    }
}
