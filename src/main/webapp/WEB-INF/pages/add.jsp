<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>UnitBean - Добавление статьи</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>">
    <link rel='stylesheet' type="text/css" href=<c:url value="/resources/lib/bootstrap-4.0.0/dist/css/bootstrap.min.css"/> >
    <script src="<c:url value="/resources/lib/bootstrap-4.0.0/dist/js/bootstrap.min.js" />"></script>
</head>
<body>
    <div id="add-container">
        <form:form action="/add" method="POST" modelAttribute="topic" enctype="multipart/form-data">
            <form:hidden path="id" />
            <div id="form-header">Добавление статьи</div>
            <div class="form-group">
                <label for="title">Заголовок</label>
                <form:input id="title" path="title" cssClass="form-control" required="required" />
            </div>
            <div class="form-group">
                <label for="description">Описание</label>
                <form:textarea path="content" id="description" cols="30" rows="10" class="form-control" required="required" />
            </div>
            <div class="form-group">
                <div >Прикрепить изображение</div>
                <label for="file-upload" class="custom-file-upload">
                    <i class="fa fa-cloud-upload"></i>
                    <img src="<c:url value="/resources/img/cam.png"/>" alt="" class="btn btn-second">
                </label>
                <input name="upload-image" id="file-upload" type="file" accept="image/*"/>
                <input type="submit" value="Добавить" class="btn btn-bean float-right" />
                <a href="<c:url value="/"/>" class="btn btn-second float-right">Отмена</a>
            </div>
        </form:form>
    </div>
</body>
</html>
