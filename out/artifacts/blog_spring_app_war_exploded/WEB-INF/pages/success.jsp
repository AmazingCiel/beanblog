<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>UnitBean - статья добавлена</title>
    <link rel='stylesheet' type="text/css" href=<c:url value="/resources/lib/bootstrap-4.0.0/dist/css/bootstrap.min.css"/> >
    <script src="<c:url value="/resources/lib/bootstrap-4.0.0/dist/js/bootstrap.min.js" />"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>">
</head>
<body>
    <div class="container">
        <img id="logo-success" src="<c:url value="/resources/img/bean.png" />" alt="UnitBean logo">
        <div class="text-bean" id="text-success">Поздравляем Вас с успешным добавлением статьи в блог UnitBean!</div>
    </div>
</body>
</html>
