<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>UnitBean - Все статьи</title>
    <link rel='stylesheet' type="text/css" href=<c:url value="/resources/lib/bootstrap-4.0.0/dist/css/bootstrap.min.css"/> >
    <script src="<c:url value="/resources/lib/bootstrap-4.0.0/dist/js/bootstrap.min.js" />"></script>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
</head>
<body>
    <div class="container" id="topic-container">
        <div id="header">
            <div id="black-logo">
                <img src="<c:url value="/resources/img/black-logo.png"/>" alt=""/>
            </div>
            <a href="<c:url value="/add"/>" class="text-bean" id="add-button">Добавить статью</a>
            <div id="header-text">Статьи, ${topicCount}</div>
        </div>
        <div>
            <c:forEach var="topic" items="${topics}">
                <div class="card topic">
                    <a href="/topic/${topic.id}" class="card-header">
                        <img src="<c:url value="${topic.fullImagePath}"/>" alt="">
                    </a>
                    <div class="card-body">
                        <div class="topic-title">
                            ${topic.title}
                        </div>
                        ${topic.formatedDate}
                    </div>
                    <div class="card-footer topic-content">
                            ${topic.content}
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</body>
</html>
