<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>UnitBean - ${topic.title}</title>
    <link rel='stylesheet' type="text/css" href="<c:url value="/resources/lib/bootstrap-4.0.0/dist/css/bootstrap.min.css"/>" />
    <script src="<c:url value="/resources/lib/bootstrap-4.0.0/dist/js/bootstrap.min.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/style.css"/>" />
</head>
<body>
    <div id="detail-container" class="container">
        <div class="card border-0">
            <div class="card-header border-0">
                <div class="card-title" id="detail-title">${topic.title}</div>
                <img src="<c:url value="/resources/img/${topic.imagePath}"/>" class="detail-image" alt="">
            </div>
            <div class="card-body">
                ${topic.content}
            </div>
            <div class="card-footer border-0">
                <form:form method="POST" action="/message/add" modelAttribute="newMessage">
                    <form:hidden path="id" />
                    <form:hidden path="topicId" value="${topic.id}" />
                    <div class="message-input-width">
                        <div class="message-owner-icon msg-icon ">
                            <img src="<c:url value="/resources/img/message-owner.svg"/>" alt="Owner Icon">
                        </div>
                    </div>
                    <div class="input-group message-input-size" id="message-field-group">
                        <form:input path="owner" type="text" cssClass="form-control" placeholder="Имя пользователя" />
                        <form:input path="content" type="text" cssClass="form-control" placeholder="Ваш коментарий" />
                    </div>
                    <div class="message-input-width">
                        <input type="submit" class="btn btn-bean message-input-size" value=">" id="message-submit">
                    </div>
                </form:form>
                <c:forEach var="msg" items="${messages}" >
                    <div class="message">
                        <div class="1">
                            <div class="message-owner-icon msg-icon ">
                                <img src="<c:url value="/resources/img/message-owner.svg"/>" alt="Owner Icon">
                            </div>
                        </div>
                        <div class="2">
                            <div class="3">
                                <div class="message-owner">${msg.owner}</div>
                                <div class="message-date">${msg.formatedDate}</div>
                            </div>
                            <div class="4">${msg.content}</div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</body>
</html>
